package org.tros.torgo.grails

import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured

/**
 * TODO: see if we can merge this and HOME.
 * May require having a user login somewhere else or
 * optionally log in from this page.
 */
@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
class TorgoController {

    def scriptService

    /**
     * Rest support for torgo and torgo/id
     *
     * With no id, a list of id + name of available
     * scripts are returned, otherwise if an id is
     * specified, the text of the script is returned
     *
     * @return
     */
    def index() {
        if(params.id != null && request.get) {
            Script s = scriptService.get(params.id)
            withFormat {
                json { render s as JSON }
                xml { render s as XML }
                '*' { render s as JSON }
            }
            return
        }

        //get available scripts
        //use the scriptService to handle restrictions
        def scripts = scriptService.scripts([max: Integer.MAX_VALUE])

        //just return the id and name
        def ret = []
        scripts.each {
            def item = [:]
            item["id"] =it.id
            item["name"] =it.name
            ret.add(item)
        }
        withFormat {
            html scripts: ret
            json { render ret as JSON }
            xml { render ret as XML }
            '*' { respond scripts: ret }
        }
    }
}
