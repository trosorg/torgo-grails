<html>
<head>
    <!--<meta name="layout" content="main"/>-->
    <title>Torgo</title>
    <asset:stylesheet src="/index.css?compile=false" />
    <asset:stylesheet src="/codemirror.css?compile=false" />
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
    <style>
        .CodeMirror {
            border: 1px solid #eee;
            height: auto;
            border-top: 1px solid black;
            border-bottom: 1px solid black;
        }
        .CodeMirror-selected  { background-color: blue !important; }
        .CodeMirror-selectedtext { color: white; }
        .stepped-text { background-color: #ff7; }
    </style>
    <asset:javascript src="jquery-3.1.1.min.js?compile=false" />
    <asset:javascript src="svg-pan-zoom.3.5.2.js?compile=false" />
    <script type="text/javascript" src="lib/require.js"></script>
    <script type="text/javascript" src="lib/codemirror.js"></script>
    <script type="text/javascript" src="lib/logo.js"></script>

    <script type="text/javascript">
        var editor;
        var messages = [];
        var mark = null;
        var paused = false;
        var worker = new Worker('logoWorker.js');
        var zoomTorgo;

        // Converts from degrees to radians.
        Math.radians = function (degrees) {
            return degrees * Math.PI / 180;
        };

        // Converts from radians to degrees.
        Math.degrees = function (radians) {
            return radians * 180 / Math.PI;
        };

        function stop() {
            if (worker !== null && typeof worker !== "undefined") {
                worker.terminate();
                worker = new Worker('logoWorker.js');
            }
            messages.splice(0, messages.length);
            $('#start').prop('disabled', false);
            $('#stop').prop('disabled', true);
            $('#pause').prop('disabled', true);
            $('#step').prop('disabled', true);
            $('#scriptSelect').prop('disabled', false);
            paused = false;
            pauseInterpreter(paused);
        }

        function pauseInterpreter(val) {
            paused = typeof(val) === 'undefined' ? !paused : val;

            $('#step').prop('disabled', !paused);

            if(paused) {
                $("#pause").prop('value', 'Resume');
            } else {
                $("#pause").prop('value', 'Pause');
            }
        }

        function interpreterStep() {
            requestAnimationFrame(function () { proc(false) });
        }

        function proc(paused) {
            if (paused) {
                return true;
            }

            if (messages.length > 0) {
                var e = messages.splice(0, 1)[0];
                switch (e[0]) {
                    case "interpreter listener":
                        var ctxPos = e[1];
                        if(mark !== null) {
                            mark.clear();
                        }
                        mark = editor.markText({line: ctxPos.line, ch: ctxPos.column}, {line: ctxPos.line, ch: ctxPos.stop}, {className: "stepped-text"});
                        break;
                    case "move":
                        if (!e[1].penup) {

                            var newLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
                            newLine.setAttribute('x1', e[1].penX);
                            newLine.setAttribute('y1', e[1].penY);
                            newLine.setAttribute('x2', e[2].penX);
                            newLine.setAttribute('y2', e[2].penY);
                            newLine.setAttribute("stroke", e[1].penColor)
                            $(".svg-pan-zoom_viewport").append(newLine);
                        }
                        break;
                    case "clear":
                        $("svg").css("background-color", "#FFFFFF");
                        $(".svg-pan-zoom_viewport").empty();
                        break;
                    case "print":
                        var cons = document.getElementById("console");
                        cons.value += e[1] + "\n";
                        break;
                    case "drawstring":
                        var newText = document.createElementNS('http://www.w3.org/2000/svg', 'text');
                        newText.innerHTML = e[1];
                        newText.setAttribute('x', e[2].penX);
                        newText.setAttribute('y', e[2].penY);
                        newText.setAttribute("fill", e[2].penColor);
                        var fontWeight = e[2].fontStyle.indexOf("bold") >= 0 ? "font-weight: bold; " : "";
                        var fontStyle = e[2].fontStyle.indexOf("italic") >= 0 ? "font-style:italic; " : "";
                        var textStyle = fontWeight + fontStyle + "font-size: " + e[2].fontSize + "; font-family: " + e[2].fontFamily;
                        newText.setAttribute("style", textStyle);
                        //fix angle which I think is in radians...
                        newText.setAttribute("transform", "rotate(" + Math.degrees(e[2].angle) + " " + e[2].penX + "," + e[2].penY + ")");
                        $(".svg-pan-zoom_viewport").append(newText);

                        break;
                    case "canvas":
                        $("svg").css("background-color", e[1]);
                        break;
                    case "done":
                        if (worker !== null) {
                            stop();
                        }
                        return false;
                        break;
                }
            }
            return true;
        }

        function interpret() {
            $('#start').prop('disabled', true);
            $('#stop').prop('disabled', false);
            $('#pause').prop('disabled', false);
            $('#step').prop('disabled', true);
            $('#scriptSelect').prop('disabled', true);

            var input = editor.getValue();

            requestAnimationFrame(function callProc() {
                if (proc(paused)) {
                    requestAnimationFrame(callProc);
                }
            });

            worker.addEventListener('message', function (e) {
                messages.push(e.data);
            }, false);

            var canvas = $("#canvas");
            var width = canvas.width();
            var height = canvas.height();
            worker.postMessage([input, width, height, $("#useDynamicScope").is(":checked")]); // Send data to our worker.
        }

        //use grails to populate the available scripts
        //etc.
        function getScript(id) {
            $.ajax({
                headers: {
                    Accept : "application/json; charset=utf-8",
                    "Content-Type": "application/json; charset=utf-8"
                },
                dataType: "json",
                url: "${createLink(action: 'index')}/" + id,
                success: function(data) {
                    editor.setValue(data.script);
                    var cons = document.getElementById("console");
                    cons.value = "";
                }
            }).done(function() {
            }).fail(function(err) {
                    console.log(err);
            }).always(function() {
            });
        }

        $(document).on('change', "#scriptSelect", function () {
            getScript($(this).val());
        });

        function download() {
            var svgData = '<svg width="' + $("#canvasContainer").width() + '" '
                + 'height="' + $("#canvasContainer").height() + '" '
                + 'xmlns="http://www.w3.org/2000/svg" '
                + 'xmlns:xlink="http://www.w3.org/1999/xlink" '
                + 'xmlns:ev="http://www.w3.org/2001/xml-events">';
            svgData += $(".svg-pan-zoom_viewport")[0].outerHTML + "</svg>";
            var svgBlob = new Blob([svgData], {type: "image/svg+xml;charset=utf-8"});
            var svgUrl = URL.createObjectURL(svgBlob);
            var downloadLink = document.createElement("a");
            downloadLink.href = svgUrl;
            downloadLink.download = "torgo.svg";
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }

        $(document).ready(function() {
            getScript($("#scriptSelect").val());

            $('#console').prop('readonly', true);
            $('#start').prop('disabled', false);
            $('#stop').prop('disabled', true);
            $('#pause').prop('disabled', true);
            $('#step').prop('disabled', true);
            $('#scriptSelect').prop('disabled', false);
            editor = CodeMirror.fromTextArea(document.getElementById('script'), {
                lineNumbers: true
            });
            $(".CodeMirror.cm-s-default").height($("#console").height() + 4);
            zoomTorgo = svgPanZoom('#canvas', {
                zoomEnabled: true,
                controlIconsEnabled: true,
                fit: false,
                center: false,
            });
        });

    </script>
</head>
    <body>
        <h1>Torgo Logo JavaScript Implementation</h1>
        <div>
            This is a test implementation of <a href="http://tros.org/torgo/">Torgo</a> written in JavaScript to allow use in any environment with no installation.<br />
            The code is available at <a href="https://github.com/ZenHarbinger/torgo-javascript">GitHub</a> under the Apache-2.0 LICENSE.<br />
        </div>
        <h2>Login</h2>
        <div>
            <!-- TODO: add in logic to detect already logged in user -->
            <a href="${createLink(controller: "login")}">Login</a> to save scripts.
        </div>
        <h2>
            <asset:image src="torgo-48x48.png" style="height: 40px;" alt="torgo-icon"/>
            <asset:image src="torgo-orange-and-green.svg" style="height: 40px;" alt="torgo"/>
        </h2>
        <div>
            <select id="scriptSelect">
                <g:each in="${scripts}" var="script" status="i">
                    <option value="${script.id}">${script.name}</option>>
                </g:each>
            </select>
        </div>
        <div style="width: 300px; height: 400px; float: left">
            <textarea id="script"></textarea>
        </div>
        <div>
            <textarea id="console" style="width: 300px; height: 400px"></textarea>
        </div>
        <div>
            <input id="start" type="button" value="Interpret" onclick="interpret();" />
            <label for="useDynamicScope">Use Dynamic Scope</label>
            <input id="useDynamicScope" type="checkbox" checked="checked" />
            <input id="stop" type="button" value="Stop" onclick="stop();" />
            <input id="pause" type="button" value="Pause" onclick="pauseInterpreter();" />
            <input id="step" type="button" value="Step" onclick="interpreterStep();" />
        </div>
        <a href="javascript: download()">Download SVG</a>
        <div id="canvasContainer" style="width: 960px; height: 960px; border: 1px solid black">
            <svg id="canvas" width="100%" height="100%" style="background-color: #FFFFFF"></svg>
        </div>
    </body>
</html>