package org.tros.torgo.grails

class Script {
    
    String script
    String name

    Boolean pub
    
    TorgoUser user

    static mapping = {
        script type: 'text'
    }

    static constraints = {
    }
}
