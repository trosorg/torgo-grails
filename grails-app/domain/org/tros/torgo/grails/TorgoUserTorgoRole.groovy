package org.tros.torgo.grails

import grails.gorm.DetachedCriteria
import groovy.transform.ToString

import org.codehaus.groovy.util.HashCodeHelper
import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
@ToString(cache=true, includeNames=true, includePackage=false)
class TorgoUserTorgoRole implements Serializable {

	private static final long serialVersionUID = 1

	TorgoUser torgoUser
	TorgoRole torgoRole

	@Override
	boolean equals(other) {
		if (other instanceof TorgoUserTorgoRole) {
			other.torgoUserId == torgoUser?.id && other.torgoRoleId == torgoRole?.id
		}
	}

    @Override
	int hashCode() {
	    int hashCode = HashCodeHelper.initHash()
        if (torgoUser) {
            hashCode = HashCodeHelper.updateHash(hashCode, torgoUser.id)
		}
		if (torgoRole) {
		    hashCode = HashCodeHelper.updateHash(hashCode, torgoRole.id)
		}
		hashCode
	}

	static TorgoUserTorgoRole get(long torgoUserId, long torgoRoleId) {
		criteriaFor(torgoUserId, torgoRoleId).get()
	}

	static boolean exists(long torgoUserId, long torgoRoleId) {
		criteriaFor(torgoUserId, torgoRoleId).count()
	}

	private static DetachedCriteria criteriaFor(long torgoUserId, long torgoRoleId) {
		TorgoUserTorgoRole.where {
			torgoUser == TorgoUser.load(torgoUserId) &&
			torgoRole == TorgoRole.load(torgoRoleId)
		}
	}

	static TorgoUserTorgoRole create(TorgoUser torgoUser, TorgoRole torgoRole, boolean flush = false) {
		def instance = new TorgoUserTorgoRole(torgoUser: torgoUser, torgoRole: torgoRole)
		instance.save(flush: flush)
		instance
	}

	static boolean remove(TorgoUser u, TorgoRole r) {
		if (u != null && r != null) {
			TorgoUserTorgoRole.where { torgoUser == u && torgoRole == r }.deleteAll()
		}
	}

	static int removeAll(TorgoUser u) {
		u == null ? 0 : TorgoUserTorgoRole.where { torgoUser == u }.deleteAll() as int
	}

	static int removeAll(TorgoRole r) {
		r == null ? 0 : TorgoUserTorgoRole.where { torgoRole == r }.deleteAll() as int
	}

	static constraints = {
	    torgoUser nullable: false
		torgoRole nullable: false, validator: { TorgoRole r, TorgoUserTorgoRole ur ->
			if (ur.torgoUser?.id) {
				if (TorgoUserTorgoRole.exists(ur.torgoUser.id, r.id)) {
				    return ['userRole.exists']
				}
			}
		}
	}

	static mapping = {
		id composite: ['torgoUser', 'torgoRole']
		version false
	}
}
