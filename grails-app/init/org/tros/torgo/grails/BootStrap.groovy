package org.tros.torgo.grails

class BootStrap {

    def init = { servletContext ->
        def adminRole = new TorgoRole(authority: 'ROLE_ADMIN').save()
        def userRole = new TorgoRole(authority: 'ROLE_USER').save()

        def maguirreUser = new TorgoUser(username: 'maguirre', password: 'password').save()
        def adminUser = new TorgoUser(username: 'admin', password: 'foobar').save()
        def mattUser = new TorgoUser(username: 'matta', password: 'password').save()
        
        def file1 = new File(this.class.classLoader.getResource('antlr/resource.manifest').toURI())
        def file2 = new File(this.class.classLoader.getResource('tortue/resource.manifest').toURI())
        boolean alt = true;
        file1.eachLine{
            def file1a = new File(this.class.classLoader.getResource('antlr/' + it).toURI())
//            System.out.println(it);
            new Script(script: file1a.text, name: 'antlr/' + it, user: maguirreUser, pub: alt).save()
            alt = !alt;
        }
        alt = true;
        file2.eachLine{
            def file1a = new File(this.class.classLoader.getResource('tortue/' + it).toURI())
//            System.out.println(it);
            new Script(script: file1a.text, name: 'tortue/' + it, user: mattUser, pub: alt).save()
            alt = !alt;
        }
        
//        TorgoUserTorgoRole.create maguirreUser, adminRole
//        TorgoUserTorgoRole.create mattUser, adminRole
        TorgoUserTorgoRole.create maguirreUser, userRole
        TorgoUserTorgoRole.create mattUser, userRole
        TorgoUserTorgoRole.create adminUser, adminRole

        TorgoUserTorgoRole.withSession {
            it.flush()
            it.clear()
        }

//        assert TorgoUser.count() == 1
//        assert TorgoRole.count() == 1
//        assert TorgoUserTorgoRole.count() == 1
        
    }
    def destroy = {
    }
}
