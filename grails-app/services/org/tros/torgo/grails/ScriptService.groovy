package org.tros.torgo.grails

import grails.gorm.transactions.Transactional

@Transactional
class ScriptService {

    def springSecurityService

    def scripts(def params=[max: 10, offset: 0]) {
        if(params["max"] == null) {
            params["max"] = 10
        }
        if(params["offset"] == null) {
            params["offset"] = 0
        }
        def principal = springSecurityService.principal.hasProperty("id")
        TorgoUser user = principal == null ? null : TorgoUser.get(springSecurityService.principal.id)
        def scripts
        if(user == null) {
            scripts = Script.findAllByPub(true, params)
        } else {
            scripts = Script.findAllByPubOrUser(true, user, params)
        }
        return scripts
    }

    def get(def id) {
        Script script = Script.get(id)
        def principal = springSecurityService.principal.hasProperty("id")
        TorgoUser user = principal == null ? null : TorgoUser.get(springSecurityService.principal.id)
        if(script.user == user || script.pub) {
            return script
        }
        return new Script()
    }
}
