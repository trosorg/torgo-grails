package org.tros.torgo.grails

import grails.gorm.services.Service

@Service(TorgoUser)
interface TorgoUserService {

    TorgoUser get(Serializable id)

    List<TorgoUser> list(Map args)

    Long count()

    void delete(Serializable id)

    TorgoUser save(TorgoUser torgoUser)

}