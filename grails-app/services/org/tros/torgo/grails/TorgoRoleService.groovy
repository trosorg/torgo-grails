package org.tros.torgo.grails

import grails.gorm.services.Service

@Service(TorgoRole)
interface TorgoRoleService {

    TorgoRole get(Serializable id)

    List<TorgoRole> list(Map args)

    Long count()

    void delete(Serializable id)

    TorgoRole save(TorgoRole torgoRole)

}