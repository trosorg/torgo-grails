package org.tros.torgo.grails

import grails.test.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class TorgoRoleServiceSpec extends Specification {

    TorgoRoleService torgoRoleService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new TorgoRole(...).save(flush: true, failOnError: true)
        //new TorgoRole(...).save(flush: true, failOnError: true)
        //TorgoRole torgoRole = new TorgoRole(...).save(flush: true, failOnError: true)
        //new TorgoRole(...).save(flush: true, failOnError: true)
        //new TorgoRole(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //torgoRole.id
    }

    void "test get"() {
        setupData()

        expect:
        torgoRoleService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<TorgoRole> torgoRoleList = torgoRoleService.list(max: 2, offset: 2)

        then:
        torgoRoleList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        torgoRoleService.count() == 5
    }

    void "test delete"() {
        Long torgoRoleId = setupData()

        expect:
        torgoRoleService.count() == 5

        when:
        torgoRoleService.delete(torgoRoleId)
        sessionFactory.currentSession.flush()

        then:
        torgoRoleService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        TorgoRole torgoRole = new TorgoRole()
        torgoRoleService.save(torgoRole)

        then:
        torgoRole.id != null
    }
}
