package org.tros.torgo.grails

import grails.test.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class TorgoUserServiceSpec extends Specification {

    TorgoUserService torgoUserService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new TorgoUser(...).save(flush: true, failOnError: true)
        //new TorgoUser(...).save(flush: true, failOnError: true)
        //TorgoUser torgoUser = new TorgoUser(...).save(flush: true, failOnError: true)
        //new TorgoUser(...).save(flush: true, failOnError: true)
        //new TorgoUser(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //torgoUser.id
    }

    void "test get"() {
        setupData()

        expect:
        torgoUserService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<TorgoUser> torgoUserList = torgoUserService.list(max: 2, offset: 2)

        then:
        torgoUserList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        torgoUserService.count() == 5
    }

    void "test delete"() {
        Long torgoUserId = setupData()

        expect:
        torgoUserService.count() == 5

        when:
        torgoUserService.delete(torgoUserId)
        sessionFactory.currentSession.flush()

        then:
        torgoUserService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        TorgoUser torgoUser = new TorgoUser()
        torgoUserService.save(torgoUser)

        then:
        torgoUser.id != null
    }
}
