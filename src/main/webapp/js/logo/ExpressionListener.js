/* 
 * Copyright 2015-2017 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var consts = require('../torgo/CodeConstants');
var logoListener = require('./antlr/LogoListener.js');
var antlr4 = require('../../antlr4/index');

/**
 * Expression Listener Class
 * 
 * @param scope
 * @returns {ExpressionListener}
 */
function ExpressionListener(scope) {
    this._value = [];
    this._scope = scope;
    logoListener.LogoListener.call(this); // inherit default listener
    return this;
}

// inherit default listener
ExpressionListener.prototype = Object.create(logoListener.LogoListener.prototype);
ExpressionListener.prototype.constructor = ExpressionListener;

ExpressionListener.prototype.evalCtx = function (ctx) {
    this._value.splice(0, this._value.length, []);
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, ctx);
    return this.getValue();
};

ExpressionListener.prototype.mathExpression = function (val1, val2, op) {
    var num1 = val1;
    var num2 = val2;
    switch (op) {
        case "-":
            num1 = num1 - num2;
            break;
        case "+":
            num1 = num1 + num2;
            break;
        case "*":
            num1 = num1 * num2;
            break;
        case "%":
            num1 = num1 % num2;
            break;
        case "/":
            num1 = num1 / num2;
            break;
        case "\\":
            num1 = Math.round(num1 / num2);
            break;
        case "^":
            num1 = Math.pow(num1, num2);
            break;
    }
    return num1;
};

ExpressionListener.prototype.enterExpression = function (ctx) {
    this._value.splice(0, 0, []);
};

ExpressionListener.prototype.exitExpression = function (ctx) {
    var values = this._value.splice(0, 1)[0];
    for (var ii = 1; ii < ctx.getChildCount(); ii += 2) {
        values.splice(0, 0, this.mathExpression(values.splice(0, 1)[0], values.splice(0, 1)[0], ctx.getChild(ii).getText()));
    }
    this._value[0].push(values[0]);
};

ExpressionListener.prototype.enterDeref = function (ctx) {
    var s = this._scope.get(ctx.name().STRING().getText());
    this._value[0].push(s);
};

ExpressionListener.prototype.enterNumber = function (ctx) {
    var num = Number(ctx.NUMBER().getText());
    this._value[0].push(num);
};

ExpressionListener.prototype.enterMultiplyingExpression = function (ctx) {
    this._value.splice(0, 0, []);
};

ExpressionListener.prototype.exitMultiplyingExpression = function (ctx) {
    var values = this._value.splice(0, 1)[0];
    for (var ii = 1; ii < ctx.getChildCount(); ii += 2) {
        values.splice(0, 0, this.mathExpression(values.splice(0, 1)[0], values.splice(0, 1)[0], ctx.getChild(ii).getText()));
    }
    this._value[0].push(values[0]);
};

ExpressionListener.prototype.enterPowerExpression = function (ctx) {
    if (ctx.getChildCount() > 1) {
        this._value.splice(0, 0, []);
    }
};

ExpressionListener.prototype.exitPowerExpression = function (ctx) {
    if (ctx.getChildCount() > 1) {
        var values = this._value.splice(0, 1)[0];
        for (var ii = 1; ii < ctx.getChildCount(); ii += 2) {
            values.splice(0, 0, this.mathExpression(values.splice(0, 1)[0], values.splice(0, 1)[0], ctx.getChild(ii).getText()));
        }
        this._value[0].push(values[0]);
    }
};

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

ExpressionListener.prototype.enterRandom = function (ctx) {
    this._value.splice(0, 0, []);
};

ExpressionListener.prototype.exitRandom = function (ctx) {
    var values = this._value.splice(0, 1)[0];
    var v = getRandomArbitrary(0, values[0]);
    this._value[0].push(v);
};

ExpressionListener.prototype.exitSignExpression = function (ctx) {
    var x = ctx.getChild(0).getText();
    if ("-" === x) {
        var peek = this._value[0];
        var index = peek.length - 1;
        var val = peek.splice(index, 1);
//        console.log("index: " + index);
//        console.log("val: " + val);
        val *= -1;
        peek.splice(index, 0, val);
    }
};

ExpressionListener.prototype.enterGetx = function (ctx) {
    var ret = this._scope.get(consts.TURTLE_X_VAR);
    this._value[0].push(ret);
};

ExpressionListener.prototype.enterGety = function (ctx) {
    var ret = this._scope.get(consts.TURTLE_Y_VAR);
    this._value[0].push(ret);
};

ExpressionListener.prototype.enterGetangle = function (ctx) {
    var ret = this._scope.get(consts.TURTLE_ANGLE_VAR);
    this._value[0].push(ret);
};

ExpressionListener.prototype.enterRepcount = function (ctx) {
    var ret = this._scope.get(consts.REPCOUNT_VAR);
    this._value[0].push(ret);
};

ExpressionListener.prototype.enterValue = function (ctx) {
    if (ctx.STRINGLITERAL() !== null) {
        this._value[0].push(ctx.STRINGLITERAL().getText().substring(1));
    }
};

ExpressionListener.prototype.getValue = function () {
    return this._value[0][0];
};

exports.ExpressionListener = ExpressionListener;