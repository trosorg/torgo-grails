/*
 * Copyright 2015-2017 Matthew Aguirre
 *
 * Licensed under the Apache License, Version 2.0  = function (the "License"){};
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var consts = require('./CodeConstants');

/**
 *
 * @param ctx
 * @returns {nm$_CodeBlock.CodeBlock}
 */
function CodeBlock(ctx) {
    this.ctx = ctx;
    this.commands = [];
    this._functions = {};
    this.parent = null;
    this._variables = [{}];
    return this;
}

/**
 * Add a command to the list.
 *
 * @param command
 */
CodeBlock.prototype.addCommand = function (command) {
    this.commands.push(command);
};

/**
 * Add a collection of commands to the list.
 *
 * @param commands
 */
CodeBlock.prototype.addCommands = function (commands) {
    this.commands = this.commands.concat(commands);
};

/**
 * Add an interpreter listener.
 *
 * @param listener
 */
CodeBlock.prototype.addInterpreterListener = function (listener) {};

/**
 * Get the commands to interpret.
 *
 * @return
 */
CodeBlock.prototype.getCommands = function () {
    return this.commands;
};

/**
 * Is the current block halted.
 *
 * @return true if halted, false if the monitor is null or the monitor is
 * not halted.
 */
CodeBlock.prototype.isHalted = function () {};

/**
 * Process the statement = function (s).
 *
 * @param scope
 * @return true if we should continue, false otherwise
 */
CodeBlock.prototype.process = function (scope) {
    var ret = consts.SUCCESS;
    for (var ii = 0; ii < this.commands.length; ii++) {
        if(ret === consts.SUCCESS) {
            var val = this.commands[ii].process(scope);
            ret = val;
        }
    }
    return ret;
};

/**
 * Add listener to this object.
 *
 * @param listener
 */
CodeBlock.prototype.removeInterpreterListener = function (listener) {};

/**
 * Check to see if this code block defines a function.
 *
 * @param name
 * @return
 */
CodeBlock.prototype.hasFunction = function (name) {
    return typeof (this._functions[name]) !== 'undefined';
};

/**
 * Get a function if it is defined.
 *
 * @param scope
 * @param name
 * @return
 */
CodeBlock.prototype.getFunction = function (name, scope) {
    if (typeof (scope) === 'undefined') {
        return this._functions[name];
    } else {
        return scope.getFunction(name);
    }
};

/**
 * Add a function to this code block.
 *
 * @param fn
 */
CodeBlock.prototype.addFunction = function (fn) {
//    console.log("Added Fn: " + fn.getFunctionName());
    this._functions[fn.getFunctionName()] = fn;
};

/**
 * Get the local context of this ANTLR generated parse tree stub.
 *
 * @return
 */
CodeBlock.prototype.getParserRuleContext = function () {
    return this.ctx;
};

/**
 * Check to see if there is a variable in the block.
 *
 * @param name
 * @return
 */
CodeBlock.prototype.hasVariable = function (name) {
    for(var ii = 0; ii < this._variables.length; ii++) {
        if(typeof(this._variables[ii][name]) !== 'undefined') {
            return true;
        }
    }
    return false;
};

/**
 * Set the value of the variable in the block.
 *
 * @param name
 * @param value
 */
CodeBlock.prototype.setVariable = function (name, value) {
    this._variables[0][name] = value;
};

/**
 * Get the value of a variable in the block.
 *
 * @param name
 * @return
 */
CodeBlock.prototype.getVariable = function (name) {
    for(var ii = 0; ii < this._variables.length; ii++) {
        if(typeof(this._variables[ii][name]) !== 'undefined') {
            return this._variables[ii][name];
        }
    }
    return null;
};

/**
 * Get the names of local variables.
 *
 * @return
 */
CodeBlock.prototype.localVariables = function () {
    var ret = [];
    if (this._variables.length > 0) {
        ret = Object.keys(this._variables[0]);
    }
    return ret;
};

/**
 * Get the lexical parent.
 *
 * @return
 */
CodeBlock.prototype.getParent = function () {
    return this.parent;
};

CodeBlock.prototype.push = function () {
    this._variables.splice(0, 0, {});
};

CodeBlock.prototype.pop = function () {
    this._variables.splice(0, 1);
};

/**
 * Set the lexical parent.
 *
 * @param block
 * @return
 */
CodeBlock.prototype.setParent = function (block) {
    this.parent = block;
};

exports.CodeBlock = CodeBlock;
