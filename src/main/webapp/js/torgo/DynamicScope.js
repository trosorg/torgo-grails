/* 
 * Copyright 2015-2017 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var scope = require('./Scope');

/**
 * DynamicScope
 *
 * @returns {DynamicScope}
 */
function DynamicScope() {
    this._scope = [];
    scope.Scope.call(this);
    return this;
}

// inherit default listener
DynamicScope.prototype = Object.create(scope.Scope.prototype);
DynamicScope.prototype.constructor = DynamicScope;

DynamicScope.prototype.push = function (codeblock) {
    this._scope.splice(0, 0, {});
    scope.Scope.prototype.push.call(this, codeblock);
};

DynamicScope.prototype.pop = function () {
    this._scope.splice(0, 1);
    return scope.Scope.prototype.pop.call(this);
};

DynamicScope.prototype.get = function (name) {
    var ret = null;
    for (var ii = 0; ii < this._scope.length; ii++) {
        if (this._scope[ii][name] !== undefined) {
            ret = this._scope[ii][name];
            break;
        }
    }
    if (ret === null) {
        ret = scope.Scope.prototype.get.call(this, name);
    }
    return ret;
};

DynamicScope.prototype.set = function (name, value) {
    var found = false;
    for (var ii = 0; ii < this._scope.length; ii++) {
        if (this._scope[ii][name] !== undefined) {
            this._scope[ii][name] = value;
            found = true;
            break;
        }
    }
    if (!found) {
        this._scope[0][name] = value;
    }
//        fireVariableSet(name, value);
};

DynamicScope.prototype.setNew = function (name, value) {
    this._scope[0][name] = value;
//        fireVariableSet(name, value);
};

DynamicScope.prototype.setGlobal = function (name, value) {
    scope.Scope.prototype.setGlobal.call(this, name, value);
};

/**
 * Get a function in the scope.
 *
 * @param name
 * @return
 */
DynamicScope.prototype.getFunction = function (name) {
    var stack = scope.Scope.prototype.stack.call(this);
    for (var ii = 0; ii < stack.length; ii++) {
        var cb = stack[ii];
        if (cb.hasFunction(name)) {
            return cb.getFunction(name);
        }
    }
    return null;
};

//    /**
//     * Get the names of variables.
//     *
//     * @return
//     */
//    @Override
//    public Collection<String> variables() {
//        HashSet<String> keys = new HashSet<>();
//        scope.forEach((slice) -> {
//            keys.addAll(slice.keySet());
//        });
//        return keys;
//    }
//
//    /**
//     * Get the names of variables.
//     *
//     * @param value
//     * @return
//     */
//    @Override
//    public Map<String, InterpreterValue> variablesPeek(int value) {
//        HashMap<String, InterpreterValue> keys = new HashMap<>();
//        for (int ii = scope.size() - 1; ii >= 0 && value >= 0; ii--, value--) {
//            HashMap<String, InterpreterValue> slice = scope.get(ii);
//            slice.keySet().forEach((key) -> {
//                keys.put(key, slice.get(key));
//            });//            keys.addAll(slice.keySet());
//        }
//        return keys;
//    }

exports.DynamicScope = DynamicScope;