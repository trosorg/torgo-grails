/* 
 * Copyright 2015-2017 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0  = function (the "License"){};
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var codeBlock = require('./CodeBlock');
var consts = require('./CodeConstants');

/**
 * CodeFunction
 *
 * @param ctx
 * @returns {CodeFunction}
 */
function CodeFunction(ctx) {
    codeBlock.CodeBlock.call(this, ctx);
    return this;
}

// inherit default listener
CodeFunction.prototype = Object.create(codeBlock.CodeBlock.prototype);
CodeFunction.prototype.constructor = CodeFunction;

CodeFunction.prototype.getFunctionName = function () {
    return this.ctx.name().getText();
};

CodeFunction.prototype.process = function (scope, params) {
//    console.log("Fn: " + this.getFunctionName());
    if (typeof params === 'undefined') {
        params = {};
    }
    
    scope.push(this);
    var ret = consts.SUCCESS;
    var keys = Object.keys(params);
    for(var ii = 0; ii < keys.length; ii++) {
        var key = keys[ii];
        scope.setNew(key, params[key]);
    }
    codeBlock.CodeBlock.prototype.process.call(this, scope);
    scope.pop();
    return ret;
};

exports.CodeFunction = CodeFunction;