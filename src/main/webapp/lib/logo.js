// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: http://codemirror.net/LICENSE

(function(mod) {
    if (typeof exports == "object" && typeof module == "object") // CommonJS
        mod(require("../../lib/codemirror"));
    else if (typeof define == "function" && define.amd) // AMD
        define(["../../lib/codemirror"], mod);
    else // Plain browser env
        mod(CodeMirror);
})(function(CodeMirror) {
    "use strict";

    CodeMirror.defineMode("logo", function() {
        function words(str) {
            var obj = {}, words = str.split(" ");
            for (var i = 0; i < words.length; ++i) obj[words[i]] = true;
            return obj;
        }
        // var keywords = words("and array begin case const div do downto else end file for forward integer " +
        //     "boolean char function goto if in label mod nil not of or packed procedure " +
        //     "program record repeat set string then to type until var while with");
        var keywords = words("repeat make localmake bk backward fd forward lt left rt right " +
            "cs clear cls clearscreen pu penup pd pendown ht hideturtle st showturtle print if to for end " +
            "home stop setxy getx gety getangle random pc pencolor cc canvascolor pause " +
            "ds drawstring label fontname fontsize fontstyle ");
        var atoms = {
            "bold": true,
            "plain": true,
            "italic": true,
            "bold_italic": true
        };

        var isOperatorChar = /[+\-*&%=<>!?|\/\\\[\]]/;

        function tokenBase(stream, state) {
            var ch = stream.next();
            //return HEX as an atom...
            if (ch == "#") {
                stream.eatWhile(/\d/);
                return "atom";
            }
            // if (ch == '"' || ch == "'") {
            //     state.tokenize = tokenString(ch);
            //     return state.tokenize(stream, state);
            // }
            // if (ch == "(" && stream.eat("*")) {
            //     state.tokenize = tokenComment;
            //     return tokenComment(stream, state);
            // }
            // if (/[\[\]{}\(\),\:\.]/.test(ch)) {
            //     return null;
            // }
            if (/\d/.test(ch)) {
                stream.eatWhile(/[\w\.]/);
                return "number";
            }
            if (ch == ";") {
                stream.skipToEnd();
                return "comment";
            }
            if (isOperatorChar.test(ch)) {
                stream.eatWhile(isOperatorChar);
                return "operator";
            }
            stream.eatWhile(/[\w\$_]/);
            var cur = stream.current();
            if (keywords.propertyIsEnumerable(cur)) return "keyword";
            if (atoms.propertyIsEnumerable(cur)) return "atom";
            return "variable";
        }

        function tokenString(quote) {
            return function(stream, state) {
                var escaped = false, next, end = false;
                while ((next = stream.next()) != null) {
                    if (next == quote && !escaped) {end = true; break;}
                    escaped = !escaped && next == "\\";
                }
                if (end || !escaped) state.tokenize = null;
                return "string";
            };
        }

        function tokenComment(stream, state) {
            var maybeEnd = false, ch;
            while (ch = stream.next()) {
                if (ch == ")" && maybeEnd) {
                    state.tokenize = null;
                    break;
                }
                maybeEnd = (ch == "*");
            }
            return "comment";
        }

        // Interface

        return {
            startState: function() {
                return {tokenize: null};
            },

            token: function(stream, state) {
                if (stream.eatSpace()) return null;
                var style = (state.tokenize || tokenBase)(stream, state);
                if (style == "comment" || style == "meta") return style;
                return style;
            },

            electricChars: "{}"
        };
    });

    CodeMirror.defineMIME("text/x-logo", "logo");

});
